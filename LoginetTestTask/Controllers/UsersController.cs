﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LoginetTestTaskCore;
using Microsoft.AspNetCore.Mvc;

namespace LoginetTestTask.Controllers
{
    [Route("api/[controller]")]
    public class UsersController : Controller
    {
        private readonly IUserRepo _userRepo;

        public UsersController(IUserRepo userRepo)
        {
            _userRepo = userRepo ?? throw new ArgumentNullException(nameof(userRepo));
        }

        // GET api/users
        [HttpGet]
        public async Task<User[]> Get()
        {
            return (await _userRepo.GetAll()).ToArray();
        }

        // GET api/users/5
        [HttpGet("{id}")]
        public async Task<User> Get(int id)
        {
            return await _userRepo.Get(id);
        }
    }
}
