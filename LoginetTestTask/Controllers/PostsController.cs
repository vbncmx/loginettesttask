﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LoginetTestTaskCore;
using Microsoft.AspNetCore.Mvc;

namespace LoginetTestTask.Controllers
{
    [Route("api/[controller]")]
    public class PostsController : Controller
    {
        private readonly IPostRepo _postRepo;

        public PostsController(IPostRepo postRepo)
        {
            _postRepo = postRepo ?? throw new ArgumentNullException(nameof(postRepo));
        }

        // GET api/posts
        [HttpGet]
        public async Task<Post[]> Get()
        {
            return (await _postRepo.GetAll()).ToArray();
        }

        // GET api/posts/5
        [HttpGet("{id}")]
        public async Task<Post> Get(int id)
        {
            return await _postRepo.Get(id);
        }

        // GET api/userposts/5
        [HttpGet("ofuser/{userId}")]
        public async Task<Post[]> ByUserId(int userId)
        {
            return (await _postRepo.GetByUserId(userId)).ToArray();
        }

    }
}
