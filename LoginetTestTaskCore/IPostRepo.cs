﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace LoginetTestTaskCore
{
    public interface IPostRepo
    {
        Task<IEnumerable<Post>> GetAll();
        Task<Post> Get(int id);
        Task<IEnumerable<Post>> GetByUserId(int userId);
    }
}