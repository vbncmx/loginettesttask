﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace LoginetTestTaskCore
{
    public interface IUserRepo
    {
        Task<IEnumerable<User>> GetAll();
        Task<User> Get(int userId);
    }
}