﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using LoginetTestTaskCore;
using Newtonsoft.Json;

namespace LoginetTestTaskData
{
    public class TypicodeUserRepo : IUserRepo
    {
        private readonly HttpClient _httpClient;

        public TypicodeUserRepo(HttpClient httpClient)
        {
            _httpClient = httpClient ?? throw new ArgumentNullException(nameof(httpClient));
        }

        public async Task<IEnumerable<User>> GetAll()
        {
            using (var response = await _httpClient.GetAsync("https://jsonplaceholder.typicode.com/users"))
            {
                var responseJson = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<User[]>(responseJson);
            }
        }

        public async Task<User> Get(int userId)
        {
            using (var response = await _httpClient.GetAsync($"https://jsonplaceholder.typicode.com/users/{userId}"))
            {
                var responseJson = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<User>(responseJson);
            }
        }
    }
}
