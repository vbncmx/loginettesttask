﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using LoginetTestTaskCore;
using Newtonsoft.Json;

namespace LoginetTestTaskData
{
    public class TypicodePostRepo : IPostRepo
    {
        private readonly HttpClient _httpClient;

        public TypicodePostRepo(HttpClient httpClient)
        {
            _httpClient = httpClient ?? throw new ArgumentNullException(nameof(httpClient));
        }

        public async Task<IEnumerable<Post>> GetAll()
        {
            using (var response = await _httpClient.GetAsync("https://jsonplaceholder.typicode.com/posts"))
            {
                var responseJson = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<Post[]>(responseJson);
            }
        }

        public async Task<Post> Get(int id)
        {
            using (var response = await _httpClient.GetAsync($"https://jsonplaceholder.typicode.com/posts/{id}"))
            {
                var responseJson = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<Post>(responseJson);
            }
        }

        public async Task<IEnumerable<Post>> GetByUserId(int userId)
        {
            return (await GetAll()).Where(p => p.UserId == userId);
        }
    }
}